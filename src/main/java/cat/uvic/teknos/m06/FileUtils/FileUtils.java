/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.uvic.teknos.m06.FileUtils;

import java.io.File;
import java.nio.file.Files;

/**
 *
 * @author Sergi
 */
public class FileUtils implements FileInterface{

    @Override
    public Boolean isValidDirectory(String path) {
        if (path==null) //First check if the value is null, otherwise it would throw an exception
            return false;
        else { //If the path is not null...
            File file = new File(path); //Create a File object with the path
            return file.isDirectory() && file.list().length == 0 && !file.getName().contains(" ") && Files.isWritable(file.toPath());
            // 1. Check if the path is a Directory, and if it exists
            // 2. Check if it's empty
            // 3. Check if the DIrecoty name has any spaces
            // 4. Check if it's writable
        }
        
    }
    
}
