/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cat.uvic.teknos.m06.FileUtils;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.EnabledOnOs;
import org.junit.jupiter.api.condition.OS;

/**
 *
 * @author Sergi
 */
public class FileUtilsTest {
    
    public FileUtilsTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of isValidDIrectory method, of class FileUtils.
     */
    @Test
    public void GivenNotExistingFile_WhenIsValidDirectory_thenFalse() {
        var fileUtils = new FileUtils();
        
        assertFalse(fileUtils.isValidDirectory("ughjk"));
    }
    
    @Test
    public void GivenFile_WhenIsValidDirectory_thenFalse() {
        var fileUtils = new FileUtils();
        
        assertFalse(fileUtils.isValidDirectory("src/test/Resources/file.txt"));
    }
    
    @Test
    public void GivenFolderNotEmpty_WhenIsValidDirectory_thenFalse() {
        var fileUtils = new FileUtils();
        
        assertFalse(fileUtils.isValidDirectory("src/test/Resources/folder1"));
    }
    
    @Test
    public void GivenFolderWithSpaces_WhenIsValidDirectory_thenFalse() {
        var fileUtils = new FileUtils();
        
        assertFalse(fileUtils.isValidDirectory("src/test/Resources/folder 2"));
    }
    
    @Test
    public void GivenNull_WhenIsValidDirectory_thenFalse() {
        var fileUtils = new FileUtils();
        
        assertFalse(fileUtils.isValidDirectory(null));
    }
    
    
    @Test
    public void GivenValidDirectory_WhenIsValidDirectory_thenTrue() {
        var fileUtils = new FileUtils();
        
        assertTrue(fileUtils.isValidDirectory("src/test/Resources/folder"));
    }
    
    @Test
    @EnabledOnOs({OS.WINDOWS})
    public void GivenNoPermissiondDirectory_WhenIsValidDirectory_thenFalse() {
        var fileUtils = new FileUtils();
        
        assertFalse(fileUtils.isValidDirectory("C:\\Windows\\System32"));
    }
    
}
